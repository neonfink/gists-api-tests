"""
Collection of Gists API tests using 'pytest' library for tests execution
"""

from apis.gists import Gists
from utils.helpers import is_file_available_in_gist

class TestRetrieveGistsValidAuthContext:
    client = Gists.git_client_v3()

    def test_get_users_gists_with_timestamp(self):
        response = self.client.get_specified_username_gists(username="neonfink", from_timestamp="2018-12-01T00:00:00Z")
        assert response.status_code == 200
        assert len(response.json()) > 0
        assert is_file_available_in_gist("testgist_1.json", response.json())

    def test_get_users_gists_without_timestamp(self):
        response = self.client.get_specified_username_gists(username="neonfink")
        assert response.status_code == 200
        assert len(response.json()) > 0
        assert is_file_available_in_gist("testgist_1.json", response.json())

    def test_get_users_gists_from_future_date(self):
        response = self.client.get_specified_username_gists(username="neonfink", from_timestamp="2050-01-01T00:00:00Z")
        assert response.status_code == 200
        assert len(response.json()) == 0

    def test_get_invalid_username_gists(self):
        response = self.client.get_specified_username_gists(username="UsernameThatDefinitelyDoesNotExist")
        assert response.status_code == 404

    def test_get_gists_for_current_auth_context(self):
        response = self.client.get_authenticated_user_gists()
        assert response.status_code == 200
        assert len(response.json()) > 0
        assert is_file_available_in_gist("testgist_1.json", response.json())


class TestRetrieveGistsWithoutAuth:
    client = Gists.git_client_v3(token=None)

    def test_get_all_public_gists_implicitly(self):
        response = self.client.get_authenticated_user_gists()
        assert response.status_code == 200
        assert len(response.json()) == 30

    def test_get_all_public_gists_explicitly(self):
        response = self.client.get_public_gists()
        assert response.status_code == 200
        assert len(response.json()) == 30

    def test_get_users_private_gists_anonymously(self):
        response = self.client.get_specified_username_gists(username="neonfink")
        assert response.status_code == 200
        assert not is_file_available_in_gist("testgist_1.json", response.json())

class TestRateLimitingWithAuth:
    client = Gists.git_client_v3()

    def test_rate_limit_allowance_with_auth(self):
        response = self.client.get_authenticated_user_gists()
        assert response.status_code == 200
        assert response.headers["X-RateLimit-Limit"] == '5000'

    def test_remaining_rate_limit_change(self):
        rate_limit_values_history = []

        for i in range(2):
            r = self.client.get_authenticated_user_gists()
            assert r.status_code == 200

            rate_limit_allowed = int(r.headers["X-RateLimit-Limit"])
            rate_limit_remaining = int(r.headers["X-RateLimit-Remaining"])
            assert rate_limit_remaining < rate_limit_allowed

            rate_limit_values_history.append(rate_limit_remaining)

        assert rate_limit_values_history[1] < rate_limit_values_history[0]

class TestRateLimitingWithoutAuth:
    client = Gists.git_client_v3(token=None)

    def test_rate_limit_without_auth(self):
        response = self.client.get_authenticated_user_gists()
        assert response.status_code == 200
        assert response.headers["X-RateLimit-Limit"] == '60'
        assert int(response.headers["X-RateLimit-Remaining"]) < int(response.headers["X-RateLimit-Limit"])

class TestGistsCRUD:
    client = Gists.git_client_v3()

    def test_create_gist(self):
        gist_description = "Test Description"
        gist_files = {
            "test_file.txt": {
                "content": "Test Content"
            }
        }
        response = self.client.post_gist(files=gist_files, description=gist_description, public=False)
        assert response.status_code == 201
        self.client.created_gist_id = response.json()["id"]

    def test_get_created_gist(self):
        response = self.client.get_single_gist(self.client.created_gist_id)
        assert response.status_code == 200
        assert response.json()["description"] == "Test Description"

    def test_delete_gist(self):
        response = self.client.delete_gist(self.client.created_gist_id)
        assert response.status_code == 204

    def test_request_non_existent_gist(self):
        response = self.client.get_single_gist(self.client.created_gist_id)
        assert response.status_code == 404

    def test_delete_non_existent_gist(self):
        response = self.client.get_single_gist(self.client.created_gist_id)
        assert response.status_code == 404

    def test_create_gist_with_empty_file_field(self):
        gist_description = "Test Description"
        gist_files = {}
        response = self.client.post_gist(files=gist_files, description=gist_description, public=False)
        print(response.content)
        assert response.status_code == 422

class TestGistsCrudWithoutAuth:
    client = Gists.git_client_v3(token=None)

    def test_create_gist_without_auth(self):
        gist_description = "Test Description"
        gist_files = {
            "test_file.txt": {
                "content": "Test Content"
            }
        }
        response = self.client.post_gist(files=gist_files, description=gist_description, public=False)
        assert response.status_code == 401

class TestRequestsWithInvalidToken:
    client = Gists.git_client_v3(token="1bbaaf6ff96a875bd9780644282fadcf")

    def test_get_gists_with_invalid_token(self):
        response = self.client.get_authenticated_user_gists()
        assert response.status_code == 401

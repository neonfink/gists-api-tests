def is_file_available_in_gist(filename, response_json):
    """Try to find an occurrence of property that matches provided filename value in the Gists response JSON
    and return a boolean value based on the result.
    """
    try:
        for item in response_json:
            if filename in item["files"]:
                return True
        return False
    except TypeError:
        print("Expected JSON Array as a 'response_json' parameter input")
        return False
# gists-api-tests

Test project to demonstrate example of an API test framework based on GitHub's Gists API using Python and pytest library.

API endpoints wrapper functions are defined in [apis.gists.py](/apis/gists.py) module, 
based on GitHub's API documentation and specifications.
Main idea is to keep any logic and network requests definitions outside of the actual test files.

For testing purposes I am using my personal GitHub account access token with 'gist' access scope, so 
tests included in this repository rely on the data accessible to that token only (e.g. private gists).
You can check full tests execution results output in `Pipelines` tab of this Bitbucket repository.

#### Prerequisites:
- Python 3.6.* or higher
- pip
- Set `$GIT_GIST_TOKEN` environment variable with personal token value that has 'gist' access scope 

External Python packages can be installed with the following command from the root directory of the project:

- `pip3 install -r requirements` (MacOS specific)

- `pip install - r requirements` (Windows specific)

If you need more details on setting up Python environment or pip please refer to one of the following links
[MacOS](http://docs.python-guide.org/en/latest/starting/install3/osx/), [Windows](https://docs.python-guide.org/starting/install3/win/)

#### Running tests:
Execute this command to run all tests and generate HTML report in the project's directory.

`pytest --html=./target/report.html`

Example report is included in _/target_ directory of this repository.

#### Following test cases are covered:

1. `TestRetrieveGistsValidAuthContext class` (issuing GET requests with valid personal token)
	- Get available gists within current user context from a specific username and specifying timestamp for filtering
	- Get available gists within current user context from a specific username, without specifying any timestamp
	- Request user's gists with timestamp of a future date, should not return anything
	- Request gists from an invalid username
	- Get available gists within current user context, without specifying username explicitly

2. `TestRetrieveGistsWithoutAuth class` (issuing GET requests anonymously)
	- Implicitly get public gists outside of authenticated context
	- Explicitly request public gists only
	- Verify that private gists are not available outside of authenticated context

3. `TestRateLimitingWithAuth class`
	- Verify that authenticated context rate limits are being applied to the requested endpoints
	- Verify that remaining rate limit is getting decreased after issuing consequent requests

4. `TestRateLimitingWithoutAuth class`
	- Verify that unauthenticated context rate limits are being applied to the requested endpoints

5. `TestGistsCRUD class`
	- Create new gist
	- Retrieve newly created gist
	- Deleted gist
	- Verify that deleted gist is no longer accessible 
	- Verify that attempt to delete non-existent gist ID is handled
	- Verify that request payload for creation of new gist is being validated by API

6. `TestGistsCrudWithoutAuth class`
	- Verify that creating new gists is not possible outside of authenticated context

7. `TestRequestsWithInvalidToken class`
	- Verify that API rejects authenticated requests with invalid tokens
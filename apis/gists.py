"""Test client implementation for GitHub Gists API

This module contains functions for issuing requests and interacting with GitHub Gists API endpoints.

Internal HTTP wrappers are based on 'requests' library and are only intended to be used inside of the module
for easier access to the 'Gists' class level properties.

API wrapper functions are implemented in a way that actual tests will require as less setup as possible and ideally
would only need to provide necessary API data as function parameters.
API wrapper functions are expected to always have at least an API endpoint defined, send a request conforming
to GitHub API documentation and return a response object.

For more information about Gists API see:
https://developer.github.com/v3/gists/

Intended usage in tests: import 'Gists' class in the tests module file and create a test client class instance
by assigning 'git_client_v3' to a variable of your choice. Any REST calls should modeled and performed using
API wrapper functions.

API token can be set to None while instantiating the client or by modifying the property
of an existing class instance.

Example:
    from apis.gists import Gists
    client = Gists.git_client_v3()

    Executing test client functions:
    client.get_public_gists()

    Accessing class properties:
    client.base_url
"""

import requests
import os

class Gists:
    def __init__(self, auth_token, api_version):

        self.auth_token = auth_token
        self.base_url = "https://api.github.com"
        self.api_version_header = api_version
        self.default_headers = { 'Accept': self.api_version_header }

    @staticmethod
    def git_client_v3(token=os.environ.get("GIT_GIST_TOKEN")):
        """

        :param token: default value is loaded from GIT_GIST_TOKEN env variable, will be set to None if it is not found
                      explicitly set it to None to instantiate an unauthenticated test client
        :return: instance of 'Gists' class
        """
        return Gists(auth_token=token,
                     api_version="application/vnd.github.v3+json")

    ''' HTTP Requests Wrappers '''

    def __get_request(self, url, query_parameters):
        if self.auth_token:
            query_parameters.update( { 'access_token': self.auth_token } )
        print(f"GET {url}. Query: {query_parameters}")
        return requests.get(url, params=query_parameters, headers=self.default_headers)

    def __post_request(self, url, query_parameters, json_payload):
        if self.auth_token:
            query_parameters.update( { 'access_token': self.auth_token } )
        print(f"POST {url}. Query: {query_parameters}. Payload: {json_payload}")
        return requests.post(url, params=query_parameters, json=json_payload, headers=self.default_headers)

    def __delete_request(self, url, query_parameters):
        if self.auth_token:
            query_parameters.update( { 'access_token': self.auth_token } )
        print(f"DELETE {url}. Query: {query_parameters}")
        return requests.delete(url, params=query_parameters, headers=self.default_headers)

    ''' Gists API Endpoints Wrappers '''

    def get_authenticated_user_gists(self, from_timestamp=None):
        """Get gists available for current user context.

        Keyword arguments:
        from_timestamp -- timestamp to look up the gists from (default None)

        """
        if from_timestamp:
            query = { 'since': from_timestamp }
        else:
            query = {}
        return self.__get_request(f"{self.base_url}/gists", query)

    def get_specified_username_gists(self, username, from_timestamp=None):
        """Get public gists.

        Keyword arguments:
        username -- Git username to get gists from (default empty)
        from_timestamp -- timestamp to look up the gists from (default None)

        """
        if from_timestamp:
            query = { 'since': from_timestamp }
        else:
            query = {}
        return self.__get_request(f"{self.base_url}/users/{username}/gists", query)

    def get_public_gists(self, from_timestamp=None):
        """Get public gists.

        Keyword arguments:
        from_timestamp -- timestamp to look up the gists from (default None)

        """
        if from_timestamp:
            query = { 'since': from_timestamp }
        else:
            query = {}
        return self.__get_request(f"{self.base_url}/gists/public", query)

    def get_single_gist(self, gist_id):
        """Get single gist by provided ID.

        Keyword arguments:
        gist_id -- ID of the gist (default empty)

        """
        query = {}
        return self.__get_request(f"{self.base_url}/gists/{gist_id}", query)

    def post_gist(self, files, description, public=False):
        """Create new gist with provided files and description.

        Keyword arguments:
        files -- filenames and content for new gist (default empty)
        description -- description for new gist (default empty)
        public -- flag to mark new gist as public (default False)
        """
        query = {}
        payload = {
            "description": description,
            "public": public,
            "files": files
        }
        return self.__post_request(f"{self.base_url}/gists", query, payload)

    def delete_gist(self, gist_id):
        """Delete gist with provided ID.

        Keyword arguments:
        gist_id -- ID of the gist (default empty)

        """
        query = {}
        return self.__delete_request(f"{self.base_url}/gists/{gist_id}", query)